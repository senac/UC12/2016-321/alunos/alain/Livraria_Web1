
package br.com.senac.livraria.bean;

import br.com.senac.livraria.banco.LivroDAO;
import br.com.senac.livraria.entity.Livro;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "livroBean")
@ViewScoped
public class LivroBean extends Bean{
    
    private Livro livro;
    private LivroDAO dao;
    
    public LivroBean() {
    }
    
    @PostConstruct
    public void init() {
        this.livro = new Livro();
        this.dao = new LivroDAO();
    }
    
    public String getCodigo() {
        return this.livro.getId() == 0 ? "" : String.valueOf(this.livro.getId());
    }
    
    public void novo() {
        this.livro = new Livro();
    }
    
    public void salvar() {
        
        try {
            
            if (this.livro.getId() == 0) {
                dao.save(livro);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(livro);
                addMessageInfo("Alterado com sucesso!");
            }
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
    
    public void excluir(Livro livro) {
        try {
            dao.delete(livro.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }
    
    public Livro getLivro() {
        return livro;
    }
    
    public void setLivro(Livro livro) {
        this.livro = livro;
    }
    
    public List<Livro> getLista() {
        return this.dao.findAll();
    }
    
}

