package br.com.senac.livraria.bean;

import br.com.senac.livraria.banco.GeneroDAO;
import br.com.senac.livraria.entity.Genero;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "generoBean")
@RequestScoped
public class GeneroBean {
     
    private Genero genero = new Genero() ; 
    private GeneroDAO dao = new GeneroDAO(); 

    public GeneroBean() {
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }
    
    public void salvar(){
        
        try{
            if(this.genero.getId() == 0 ){
                dao.save(genero);
            }else{
                dao.update(genero);
            }            
            //Salvo com sucesso
            FacesContext context = FacesContext.getCurrentInstance() ; 
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo", "Salvo com sucesso") ; 
            context.addMessage("dasdasasd", message);
           
        }catch(Exception ex){
            ///deu erro 
        }
     
    }
    
    public void novo(){
        this.genero = new Genero();
    }
    
    public List<Genero> getLista(){
        return dao.findAll();
    }  
    
}
